#!/usr/bin/env python3

import requests
from bs4 import BeautifulStoneSoup
import warnings
# from blessings import Terminal
from dearpygui import dearpygui

warnings.filterwarnings("ignore", category=UserWarning, module='bs4')





# t =Terminal()


def num(s):
    try:
        return int(s.split()[0])
    except ValueError:
        return float(s.split()[0])


# def printout(txt):
#     used, total = txt.split('/')
#     print(f'Used :{t.white_bold_on_red}{used} of {total}{t.normal}')


def get_data():
	headers_mobile = { 'User-Agent' : 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B137 Safari/601.1'}
	req = requests.get('http://mydata.du.ae/queryData/init', headers=headers_mobile)
	soup = BeautifulStoneSoup(req.text, features="xml")
	data = soup.find_all('span', class_="dtb")
	#for v in data:
	used, total = data[0].get_text().split('/')
	balance = num(total) - num(used)
	return num(used), num(total), balance




dearpygui.set_main_window_size(width=450, height=150)
#dearpygui.set_global_font_scale(scale=1)
dearpygui.add_text('My cell data balance')
used, total, balance = get_data()
txt = f'Total: {total}GB Used: {used}GB Balance:{balance}GB'
print(txt)
dearpygui.add_spacing(count=3)
dearpygui.add_label_text(name='datatext', value=txt)
dearpygui.add_spacing(count=3)
dearpygui.add_progress_bar('balance_progress',value=used/total, width=350)
dearpygui.add_spacing(count=5)
dearpygui.add_button("Refresh", callback="refresh_callback")

def refresh_callback(sender, data):
	dearpygui.set_value(name='datatext', value='Getting data from the web ...')
	dearpygui.set_value(name='balance_progress', value=0.1)
	used, total, balance = get_data()
	txt = f'Total: {total}GB Used: {used}GB Balance:{balance}GB'
	dearpygui.set_value(name='datatext', value=txt)
	for i in range(0,used/total,.1):
		dearpygui.set_value(name='balance_progress', value=i)

dearpygui.start_dearpygui()
	
