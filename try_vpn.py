#! /usr/bin/env python3

''' Docs:
Try VPN:
    This script connect to expressvpn and test the connections
'''

import subprocess
import shlex
from blessings import Terminal
from pathlib import Path
import time
import os
from random import choice
import urllib.request
import urllib.error


t = Terminal()
logfile = Path('/home/sultan/Projects/vpn/log.txt')
con_result = []


def check_internet():
    '''
    Checking the internet connection after connecting to vpn.
        return True/False
    '''
    print(f'{t.yellow_bold}Checking internet connection ....')
    try:
        url = urllib.request.urlopen('http://www.google.com', timeout=20)
        print(f'{t.green_bold} connection test passed :){t.normal}')
        return True
    except urllib.error.URLError as err:
        pass
        print(f'{t.red} Connection test faild ... {t.normal}')
        return False


def printtable(lst):
    # Printing list of all failed connections.
    for line in lst:
        print(f'  {line[0]:<7} | {line[1]:>7}')


def run(command):
    command = shlex.split(command)
    process = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE)
    process.wait()
    # print(process.communicate())
    return process.communicate()[0].decode('UTF-8')


def vpns_list():
    # Get list of servers.
    cmd = 'expressvpn list'
    result = run(cmd)
    txt = result.split('\n')
    listvpns = []
    for line in txt[5:-3]:
        vpn = line.split('\t')[0]
        listvpns.append(vpn)
    return listvpns


def disconnect(location):
    # Disconnect vpn.
    print(f'Disconnecting from {location}.')
    run('expressvpn disconnect')
    print('Removing log file.')
    run('rm ' + logfile)


def connect(location):
    '''
    Connect to vpn:
        :param locations: string, sever name from vpn list.
    '''
    command = '{} {}'.format('expressvpn connect', location)
    print(t.yellow_bold, command, t.normal)
    result = run(command)
    if 'Connected' in result:
        print(t.green_bold, 'Connected ... ', t.normal)
        if check_internet():
            logfile.write_text(location)
            exit()
        else:
            disconnect(location)
    else:
        print(t.red_bold, ' *** Faild to connect ***', t.normal)
        con_result.append([location, 'Faild'])
        time.sleep(2)
        # disconnect()
        os.system('clear')
        printtable(con_result)
        print()


def start(vpnlist):
    '''
    Start the connection:
        :param vpnlist: List, list of severs names.

    '''
    for x in range(len(vpnlist)):
        result = choice(vpnlist)
        vpnlist.remove(result)
        connect(result)
    # for location in vpnlist:
    #     connect(location)


if __name__ == '__main__':
    # check if random

    # first try previos location.
    if logfile.is_file():
        connect(logfile.read_text())
    # then try from the old list
    vpnlist = vpns_list()
    start(vpnlist)
