
''' Docs:

Description : pymenu a very simple menue.

Example of using this menu :

import pymenu

def new_file(fname): print('New {} file Created'.format(fname))
def edit_file(): print('file edited')
def delet_file(): print('file deleted')

mainmenu = pymenu.pyMenu()
mainmenu.title='My Main Menu'
mainmenu.add('Newfile','starting new file.','new_file',{'fname':'samplefile.txt'})
mainmenu.add('Editfile','starting new file.','edit_file')
mainmenu.add('Deletfile','starting new file.','delet_file')
mainmenu.show()

'''

from collections import OrderedDict
from colorama import Fore, Style
import os
import time


class pyMenu():

    def __init__(self):
        self.menu = OrderedDict()
        self.title = 'Main Menu'

    def add(self, name, desc, func, arg=None):
        uname = len(self.menu) + 1
        self.menu[uname] = {'name': name,
                            'desc': desc, 'func': func, 'arg': arg}

    def runfun(self, func_name, func_arg):
        # importing local file
        os.system('clear')

        print('\n')

        import __main__

        func_name = getattr(__main__, func_name)  # getting the function name
        if func_arg == None:
            func_name()
        else:
            func_name(**func_arg)

        c = input('\n\nPress any key to continue')

    def show(self):

        while True:
            os.system('clear')
            print('{}{}\n{:^40}\n{}{}'.format(
                Fore.CYAN, ('=' * 40), self.title, ('=' * 40), Fore.RESET))

            for itm in self.menu.keys():

                print('{}[{}]  {}{:<16}{}{:<30}'.format(Fore.GREEN, itm, Fore.CYAN, self.menu[
                      itm]['name'], Fore.GREEN, self.menu[itm]['desc']))

            self.opt = input('{}{}\n{}Enter your selection or q to exit : {}'.format(
                Fore.CYAN, ('=' * 40), Fore.YELLOW, Fore.RESET))

            if self.opt == 'q' or self.opt == 'Q':
                break
            else:
                # self.runfun(self.menu[int(self.opt)]['func'],self.menu[int(self.opt)]['arg'])
                try:
                    self.runfun(self.menu[int(self.opt)][
                                'func'], self.menu[int(self.opt)]['arg'])
                except Exception as e:
                    print('{}Invalid selection'.format(Fore.RED))
                    print(str(e))
                    time.sleep(.7)
                    continue
