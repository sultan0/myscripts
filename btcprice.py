#!/usr/bin/env python3
import sys
import requests
import urllib
from datetime import datetime
from blessings import Terminal
import numpy as np
import click
import time
import os
from rich.console import Console
from rich.table import Table
from rich.live import Live

MARKET_URL = "https://api.huobi.pro"
h = 'http://api.huobi.pro/market/detail/merged?symbol='
t = Terminal()
today = datetime.strftime(datetime.now(), '%d/%m/%Y')
btcbalance = 19.91251026
record_path = '/home/sultan/Projects/Btc/record.txt'


@click.group()
def cli():
    pass


def http_get_request(url, params, add_to_headers=None):
    headers = {
        "Content-type": "application/x-www-form-urlencoded",
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
    }
    if add_to_headers:
        headers.update(add_to_headers)
    postdata = urllib.parse.urlencode(params)
    response = requests.get(url, postdata, headers=headers, timeout=5)
    try:

        if response.status_code == 200:
            return response.json()
        else:
            return
    except BaseException as e:
        print("httpGet failed, detail is:%s,%s" % (response.text, e))
        return


def get_kline(symbol, period, size=150):
    """
    :param symbol
    :param period：{1min, 5min, 15min, 30min, 60min, 1day, 1mon, 1week, 1year }
    :param size: [1,2000]
    :return:
    """
    params = {'symbol': symbol,
              'period': period,
              'size': size}

    url = MARKET_URL + '/market/history/kline'
    return http_get_request(url, params)


def pc(oldprice, newprice):
    ''' Price Color : This func will show arrow up and down '''
    if oldprice > newprice:
        mark = ''
        row_style = 'bold'
    else:
        mark = ''
        row_style = None
    return mark, row_style


def to_aed(amount):
    return amount * 3.673


def getprice(symbol='btc'):
    """
    :param symbol: 
    :return:
    """
    params = {'symbol': symbol + 'usdt'}

    url = MARKET_URL + '/market/detail/merged'
    response = http_get_request(url, params)
    return response['tick']['close']


def record(btcusd, btcaed_total):
    with open(record_path, 'a') as recordfile:
        recordfile.write(f'{today}:{btcusd}:{btcaed_total}\n')


def getrecord():
    with open(record_path, 'r') as recordfile:
        lastline = recordfile.readlines()[-1]
    return lastline


def avg():
    with open(record_path, 'r') as recordfile:
        result = recordfile.readlines()
    result = [x.replace('\n', '') for x in result]
    btcusd_list = [float(x.split(':')[1].replace(',', '')) for x in result]
    totalaed_list = [float(x.split(':')[2].replace(',', '')) for x in result]
    return np.mean(btcusd_list), np.mean(totalaed_list)


def lossgain(old, new):
    net = new - old
    if net > 0:
        return f'Gain : {t.green_bold}{net:,.2f}{t.normal}'
    else:
        return f'Loss :{t.red_bold}{net:,.2f}{t.normal}'


def result_out(symbol, data):
    # printing the result
    # print(f'{t.white_bold_on_blue}{"Balance":^40}{t.normal}')
    print()
    print(f'    {t.green}{symbol.upper()} price: {t.yellow}{data[f"{symbol}usd"]:,.2f}$')
    print(f'    {t.green}{symbol.upper()} volume: {t.yellow}{data[f"{symbol}balance"]}')
    print(f'    {t.green}Total USD: {t.yellow}{data[f"{symbol}usd_total"]:,.2f}$')
    print(f'    {t.green_bold}Total AED: {t.yellow}{data[f"{symbol}aed_total"]:,.2f}AED{t.normal}')
    print(f'    Date : {today}')


@cli.command(help='How many crypto you can get against AED.')
@click.argument('amount', required=True)
@click.argument('symbol', default='btc')
def aed(symbol, amount):
    symbol = symbol.lower()
    price = float(getprice(symbol))
    print(f'For {t.yellow_bold}{float(amount):,.2f} AED{t.normal}' +
          f' you will get {t.yellow_bold}{(float(amount) / 3.65) / price:,.4f}' +
          f'{t.green_bold}{symbol.upper()}{t.normal}')


@cli.command(help='How many crypto you can get against USD.')
@click.argument('amount', required=True)
@click.argument('symbol', default='btc')
def usd(symbol, amount):
    symbol = symbol.lower()
    price = float(getprice(symbol))
    print(f'For {t.yellow_bold}{float(amount):,.2f} USD{t.normal}' +
          f' you will get {t.yellow_bold}{float(amount) / price:,.4f}' +
          f' {t.green_bold}{symbol.upper()}{t.normal}')


def main(symbol='btc', btcbalance=btcbalance):
    symbol = symbol.lower()
    data = {}
    data[f'{symbol}balance'] = btcbalance
    data['aed'] = 3.65
    data[f'{symbol}usd'] = float(getprice(symbol))
    data[f'{symbol}usd_total'] = data[f'{symbol}usd'] * btcbalance
    data[f'{symbol}aed_total'] = data[f'{symbol}usd_total'] * data['aed']
    result_out(symbol, data)


@cli.command(help='To get the price of a crypto.')
@click.argument('amount', default=btcbalance)
@click.argument('symbol', default='btc')
@click.option('--sold', '-s', help='Amount bougt for.')
def cc(symbol, amount, sold=None):
    symbol = symbol.lower()
    # print(f'This will show {symbol} price in {currency} for {amount}')
    data = {}
    data[f'{symbol}balance'] = amount
    data['aed'] = 3.65
    data[f'{symbol}usd'] = float(getprice(symbol))
    data[f'{symbol}usd_total'] = data[f'{symbol}usd'] * amount
    data[f'{symbol}aed_total'] = data[f'{symbol}usd_total'] * data['aed']
    result_out(symbol, data)
    if sold is not None:
        price_diff = data[f'{symbol}usd'] - float(sold)
        total_dif = price_diff * amount
        if total_dif > 0:
            total_dif = f'{t.green_bold}{total_dif:.2f}'
        else:
            total_dif = f'{t.red_bold}{total_dif:.2f}'
        print('    ------------ ')
        print(f'    Total gain/loss :{total_dif}  USD')


@cli.command(help='Monitor the price changes.')
@click.argument('tprice', required=True)
@click.argument('qty', default=1.00)
@click.argument('symbol', default='btc')
def mon(tprice, qty, symbol):
    oldprice = float(tprice)
    mark = '-'
    row_style = None
    with Live() as live:

        while True:
            table = Table(title=f"{symbol} monitoring table")
            table.add_column("Description", justify="Left",
                             style="cyan", no_wrap=True)
            table.add_column(f"{symbol+'/USDT'}", justify="right", style="magenta")
            table.add_column("Total USDT", justify="right", style="green")
            table.add_column("Total AED", justify="right", style="yellow")
            tprice = float(tprice)
            total_tprice = tprice * qty
            match = False
            # rich table start here
            data = get_kline(f'{symbol}usdt', '5min', '1')
            price = float(data['data'][0]['close'])
            total_price = price * qty
            diff = price - tprice
            total_diff = diff * qty
            precent = diff / price * 100
            mark, row_style = pc(oldprice, price)
            table.add_row("Current Price", f'({mark}) {price:>9,.2f}', f'{total_price:>9,.2f}', f'{to_aed(total_price):>9,.2f}', style=row_style)
            table.add_row("Targeted Price", f'{tprice:>9,.2f}', f'{total_tprice:>9,.2f}', f'{to_aed(total_tprice):>9,.2f}')
            table.add_row("The differance", f'{diff:>9,.2f}', f'{total_diff:>9,.2f}', f'{to_aed(total_diff):>9,.2f}', style=row_style)
            table.add_row('The diff %    ', f'{precent:>9,.2f}%', '', '', style=row_style)
            live.update(table)
            oldprice = price
            time.sleep(3)


if __name__ == '__main__':
    cli()
