#!/usr/bin/env python3
from tempfile import NamedTemporaryFile
import shutil
import csv
import click
#Description: remove sapces from csv data.


@click.command()
@click.option('-i','--infile', required=True, help='Csv file name')
def start(infile):
    ''' Read csv file and remove spacess '''    
    tempfile = NamedTemporaryFile('w+t', newline='', delete=False)

    with open(infile, 'r', newline='') as csvFile, tempfile:
        reader = csv.reader(csvFile, delimiter=',', quotechar='"')
        writer = csv.writer(tempfile, delimiter=',', quotechar='"')
        for row in reader:
            newrow=[]
            for item in row:
                item = item.strip()
                newrow.append(item)
            writer.writerow(newrow)

    shutil.move(tempfile.name, infile)


    
if __name__=="__main__":
    start()
