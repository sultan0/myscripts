#!/usr/bin/env python3

''' Docs:
    This script create readme file.
'''
import re
from pathlib import Path

_code = "'''"

def readfile(pyfile):
    # read file and return docs string.
    with open(pyfile) as pyfile:
        txt = pyfile.readlines()
    com = re.compile(r'', re.MULTILINE)
    docs = re.findall(r"Docs:([\w\s:.,]*)", ''.join(txt))
    return docs

def main():
    # listing all python files.
    for filename in Path('.').iterdir():
        if filename.suffix == '.py' and filename.name != 'mk_readme.py':
            print(filename.name)
            print(''.join(readfile(filename)))


if __name__=='__main__':
    main()
        


