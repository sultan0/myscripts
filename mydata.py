#!/usr/bin/env python3


''' Docs:

MyData:
    To get the data balance when using my cell data package using Hotspot.

'''

import requests
from bs4 import BeautifulStoneSoup
import warnings
from blessings import Terminal


t =Terminal()

def printout(txt):
    used, total = txt.split('/')
    print(f'Used :{t.white_bold_on_red}{used} of {total}{t.normal}')



warnings.filterwarnings("ignore", category=UserWarning, module='bs4')

headers_mobile = { 'User-Agent' : 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B137 Safari/601.1'}
req = requests.get('http://mydata.du.ae/queryData/init', headers=headers_mobile)
soup = BeautifulStoneSoup(req.text, features="xml")
data = soup.find_all('span', class_="dtb")
for v in data:
    printout( v.get_text())
